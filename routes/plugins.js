var express = require('express');
var router = express.Router();
var Engine = require('tingodb')();
assert = require('assert');
var db = new Engine.Db('db', {});

var userPluginsCol = db.collection("user_plugins");
var pluginsCol = db.collection("plugins");

// pluginsCol.drop(function(err, reply) {
// });
// userPluginsCol.drop(function(err, reply) {
// });


var newPlugins = [
{
    name: 'Speak my BANK',
    shortDesc: 'Speak my BANK',
    longDesc: 'Speak my BANK',
    path: 'speach',
    developer: "Hackaton Corporation",
    users: "123"
},
{
    name: 'Touch ID',
    shortDesc: 'Authenticate using Touch ID on your device',
    longDesc: 'Authenticate using Touch ID on your device',
    path: 'touch',
    developer: "Apple",
    users: "233"
},
{
    name: 'Uber',
    shortDesc: 'Call a cab via UBER',
    longDesc: 'Now you can call a cab from your internet banking app',
    path: 'uber',
    developer: "Uber",
    users: "4"
},
{
    name: 'Facebook transfer',
    shortDesc: 'Browse facebook friends and transfer money',
    longDesc: 'Browse facebook friends and transfer money',
    path: 'facebook',
    developer: "Facebook",
    users: "4545"
},
{
    name: 'Personal Finance Manager',
    shortDesc: 'PFM is a powerful yet easy to use personal finance app',
    longDesc: 'PFM is a powerful yet easy to use personal finance app',
    path: 'pfm1',
    developer: "InterSystems",
    users: "1234"
},
{
	name:'UNICEF - Sponsor Me!',
	shortDesc:'Sponsor a child and make the world a better place!',
	longDesc:"Many children abandon school and don't get the education they need. You can change that!",
	path:'unicef_sponsor_me',
    developer: "UNICEF",
    users: "545"
}, 
{
    name: 'Marathoner',
    shortDesc: 'Donate as much as you run!',
    longDesc: 'Marathoner is used by many marathon organizers to raise funds. Signup for a race and ',
    path: 'marathoner',
    developer: "Sport LTD",
    users: "6655"
},
{
    name: 'Buy Me Gifts',
    shortDesc: 'Planning a wedding or a birthday? Share your wishlist to contacts',
    longDesc: 'Planning a wedding or a birthday? Share your wishlist to contacts',
    path: 'buy',
    developer: "Gift SRL",
    users: "67"
}];


pluginsCol.insert(newPlugins, {w:1}, function(err, result) {
	  
});

/* GET ALL plugins. */
router.get('/', function(req, res, next) {
	
	pluginsCol.find({}).toArray(function(err, items) {
	    assert.equal(null, err);
	   	res.send(items);
  	});

});

/* GET only my installed plugins. */
router.get('/my', function(req, res, next) {
	var userId = "999";
	userPluginsCol.find({userId:userId}).toArray(function(err, items) {
	    assert.equal(null, err);
	   	res.send(items);
  	});

});

/* POST Install new plugin */
router.post('/:id/install', function(req, res) {
    
    var pluginId = req.params.id;

    //TODO: get this value samehow??? headers etc 
    var userId = "999";
    var userPlugins = {
    	userId : userId,
    	plugins : [
    	]
    };

    pluginsCol.findOne({_id:pluginId},function(err, item) {
    	
    	
    	userPluginsCol.findOne({userId:userId},function(err, dbUserPlugins) {
    		if(dbUserPlugins != null) {
    			
    			userPlugins = dbUserPlugins;
    			
    		} 
			userPlugins.plugins.push(item);

			userPluginsCol.remove({userId : userId}, {w:1}, function(err, r) {
		    	userPluginsCol.insert(userPlugins, {w:1}, function(err, result) {
					res.send(result);
				});
		    });	
    		
  		});	
    	

  	});
    
   
});

module.exports = router;
