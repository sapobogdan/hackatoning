var express = require('express');
var router = express.Router();
var http = require('http');

router.get('/me', function(req, res, next) {

    var token = req.headers['authorization'];
    console.log(req.headers.authorization);
                    //http://commonapi.hackathon2015.ing.ro-int/ES/commonapi/persons/140
    console.log(" token : "+token);
    var options = {
        host: 'commonapi.hackathon2015.ing.ro-int',
        path: '/ES/commonapi/me',
        headers: {'Authorization': 'Bearer '+token}
    };

    callback = function(response) {
        var str = '';

        //another chunk of data has been recieved, so append it to `str`
        response.on('data', function (chunk) {
            str += chunk;
        });

        //the whole response has been recieved, so we just print it out here
        response.on('end', function () {
//            console.log("----end----");
            console.log("STRING ::: "+str);
            if(str.indexOf("authorization") != -1)   {
                res.json({error: str});
            }else{
                var obj = JSON.parse(str);
                res.json(obj);
            }
        });
    }

    http.request(options, callback).end();

});


router.get('/accounts', function(req, res, next) {

    var token = req.headers['authorization'];
    console.log(req.headers.authorization);
    //http://commonapi.hackathon2015.ing.ro-int/ES/commonapi/persons/140
    console.log(" token : "+token);
    var options = {
        host: 'commonapi.hackathon2015.ing.ro-int',
        path: '/ES/commonapi/persons/140/accounts',
        headers: {'Authorization': 'Bearer '+token}
    };

    callback = function(response) {
        var str = '';

        //another chunk of data has been recieved, so append it to `str`
        response.on('data', function (chunk) {
            str += chunk;
        });

        //the whole response has been recieved, so we just print it out here
        response.on('end', function () {
//            console.log("----end----");
//            console.log(str);
            if(str.indexOf('Invalid') >= 0)   {
                res.json({error: str});
            }else{
                var obj = JSON.parse(str);
                res.json(obj);
            }
        });
    }

    http.request(options, callback).end();

});

module.exports = router;
