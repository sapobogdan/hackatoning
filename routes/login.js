/**
 * Created with JetBrains WebStorm.
 * User: adchannels
 * Date: 20/11/15
 * Time: 13:44
 * To change this template use File | Settings | File Templates.
 */


var express = require('express');
var router = express.Router();
var http = require('http');
var querystring = require('querystring');

/* GET home page. */
router.get('/auth', function(req, res, next) {
    var options = {
        host: 'commonapi.hackathon2015.ing.ro-int',
        path: '/ES/securityapi/oauth2/authorization?client_id=1&redirect_uri=localhost:3000/app/app/redirect&response_type=token&scope=&state=ES',
        //since we are listening on a custom port, we need to specify it by hand
//        port: '1337',
        //This is what changes the request to a POST request
        method: 'POST',
        headers: {
            'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding' :'gzip, deflate',
            'Accept-Language' : 'en-US,en;q=0.8,ro;q=0.6',
            'Cache-Control' : 'max-age=0',
            'Connection':'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(data),
            'Host':'commonapi.hackathon2015.ing.ro-int',
            'DNT': 1,
            'Origin':'http://commonapi.hackathon2015.ing.ro-int',
            'Referer' : 'http://commonapi.hackathon2015.ing.ro-int/ES/securityapi/oauth2/authorization?client_id=1&redirect_uri=tbd&response_type=token&scope=&state=ES',
            'Upgrade-Insecure-Requests': 1,
            'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'
        }
    };

    var data = querystring.stringify({
        username: "Julia-Avila",
        password: "Julia-Avila",
        client_id:1,
        redirect_uri:'http://localhost:3000/app/app/redirect',
        scope: '',
        state: 'ES',
        response_type: 'token',
        consent: 'Accept'
    });




    console.log(data);


    callback = function(response) {
        console.log("-- callback called --");
//        console.log(response.statusCode);
        console.log(response.headers);
        var str = '';

        //another chunk of data has been recieved, so append it to `str`
        response.on('data', function (chunk) {
            console.log("-- data received --");
            str += chunk;
        });

        //the whole response has been recieved, so we just print it out here
        response.on('end', function () {
            console.log("-----------END-----------")
            console.log(str);
            res.json(str);
        });

    }



    var request = http.request(options, callback);
//This is the data we are posting, it needs to be a string or a buffer

    request.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });


    request.write(data);

    console.log(request);


    request.end();

//    console.log(req);

});


router.get('/callback', function(req, res, next) {
    var code = req.query.code;
    console.log('/callback');
    oauth2.authCode.getToken({
        code: code,
        redirect_uri: 'http://localhost:3000/callback'
    }, saveToken);

    function saveToken(error, result) {
        if (error) { console.log('Access Token Error', error.message); }
        token = oauth2.accessToken.create(result);
    }
});

router.get('/', function(req, res, next) {
//    res.send('Hello<br><a href="/auth">Log in with Github</a>');
});

module.exports = router;
