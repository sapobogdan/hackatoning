angular.module('appApp')
    .factory('PersonService', ['$log', '$http', '$q',
        function ($log, $http, $q) {
            var personService = {
                getMe: function () {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: '/persons/me'
                    }).then(function successCallback(response) {
                            console.log("Succes");
                            console.log(response);
                            deferred.resolve(response);
                        }, function errorCallback(response) {
                            console.log("Error");
                            console.log(response);
                            deferred.reject(response);
                        });
                    return deferred.promise;
                },
                getAccounts: function(){
                        var deferred = $q.defer();
                        $http({
                            method: 'GET',
                            url: '/persons/accounts'
                        }).then(function successCallback(response) {
                                console.log("Succes");
                                console.log(response);
                                deferred.resolve(response);
                            }, function errorCallback(response) {
                                console.log("Error");
                                console.log(response);
                                deferred.reject(response);
                            });
                        return deferred.promise;
                }
            };

            return personService;

        }])
;