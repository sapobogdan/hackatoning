'use strict';

angular.module('appApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router'
])
    .config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

        //$urlRouterProvider.otherwise('/');

        $stateProvider
        .state('index', {
            url: '/',
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
        })
        .state('dashboard', {
          url: '/dashboard',
            templateUrl: 'views/dashboard.html',
            controller: 'DashboardCtrl'
        })
        .state('store', {
          url: '/store',
            templateUrl: 'views/store.html',
            controller: 'StoreCtrl'
        })
        .state('myapps', {
          url: '/myapps',
            templateUrl: 'views/myapps.html',
            controller: 'MyAppsCtrl'
        })
        .state('run', {
          url: '/run?name&path',
            templateUrl: 'views/run.html',
            controller: 'RunCtrl'
        })

        $httpProvider.interceptors.push('authInterceptor');
        // use the HTML5 History API
//        $locationProvider.html5Mode(true);
    });
