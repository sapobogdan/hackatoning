angular.module('appApp').factory('authInterceptor', ['$cookies',
    function ($cookies) {
        return {
            request: function (config) {
                console.log("INTERCEPTOR");
                var token = $cookies.token || 'test';
                config.headers['Authorization'] = token;
                config.headers['Accept'] = 'application/json;odata=verbose';

                return config;
            }
        };
    }]);


//
//.
//factory('authInterceptor', ['$log', '$cookies',
//    function ($log, $cookies) {
//        var authInterceptor = {
//            request: function (config) {
//                var token = $cookies.token || 'test';
//                var token = "sss";
//                if (token) {
//                    config.headers['Authorization'] = 'Token token="' + token + '"';
//                }
//            }
//        };
//
//        return authInterceptor;
//
//    }])
//;