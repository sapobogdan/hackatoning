'use strict';

angular.module('appApp')
    .controller('MainCtrl',['$scope', '$timeout', '$location', '$window', 'PersonService', '$q', '$cookies', '$state',
        function ($scope, $timeout, $location, $window, PersonService, $q, $cookies, $state) {

        var authPostUrl = "http://commonapi.hackathon2015.ing.ro-int/NL/securityapi/oauth2/authorization?client_id=1&redirect_uri=tbd&response_type=token&scope=&state=NL";
        //Adaugati aici orice trebuie la inceput de flow!
        function initialize() {
            var cookie = document.cookie || {};
            var token;
//            alert(document.cookie);
            if(cookie){
                console.log(cookie);
                var hash = document.cookie.substring(6, cookie.length);
                var cookies = hash.split("&");
                for(var i=0; i<cookies.length; i++){
                    var ck = cookies[i].split('=');
                    var key = ck[0];
                    var value = ck[1];
                    if(key == "access_token")    {
                        token = value;
//                        document.cookie = document.cookie + "&token="+token
                        $cookies.token = token;

                            }
                }
            }
            token = token || $cookies.token;
            if(!!token){
                PersonService.getMe()
                    .then(function(response){
                        console.log("Successss!!!");
                        console.log(response);
                    })
                console.log("Token: "+token);
                $state.go('dashboard');
            }
            $scope.user = {};
        }

        $scope.login = function () {

            var redirectUrl = 'localhost:3000/app/app';
            var authUrl = 'http://commonapi.hackathon2015.ing.ro-int/ES/securityapi/oauth2/authorization?client_id=1&redirect_uri='+redirectUrl+'&response_type=token&scope=&state=NL';
            $timeout(function(){
                window.location.href = authUrl;
            }, 1000);
        }

        initialize();
    }]);
