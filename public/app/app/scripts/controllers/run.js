'use strict';

angular.module('appApp')
    .controller('RunCtrl', function ($scope, $state, $window) {

        function initialize() {
          $scope.plugin = {
              name: $state.params.name,
              path: '/all-plugins/' + $state.params.path + '/main.html',
              slug: $state.params.path
          };
        }

        var iframe = angular.element('#appIframe');
				var iframeNode = iframe[0];

				var iframeHeight = iframeNode.scrollHeight;
				var windowHeight = angular.element($window).height();

				var spacerHeight;
				var margins = parseInt(iframe.css('margin-top')) + parseInt(iframe.css('margin-bottom'));

				try {
					spacerHeight = Number(getComputedStyle(iframeNode, '').fontSize.match(/(\d*(\.\d*)?)px/)[1]);
				} catch (ex) {
					spacerHeight = 16;
				}
				var allocatedHeight = (windowHeight - iframeNode.getBoundingClientRect().top
															 - spacerHeight * 2 -margins);

				iframe.height(allocatedHeight);


				initialize();
    });
