'use strict';

angular.module('appApp')
    .controller('DashboardCtrl', ['$scope', '$q', '$location', 'PersonService',
        function ($scope, $q, $location, PersonService) {


            function initialize() {
                $scope.accounts = [];

                PersonService.getAccounts().then(function(data){
                    $scope.accounts = data.data.list;
                    console.log($scope.accounts);
                });

            }

            initialize();
        }]);

