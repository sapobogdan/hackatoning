'use strict';

angular.module('appApp')
    .controller('MyAppsCtrl', function ($scope, $q, $location, $http, $state) {

        function initialize() {
            var token = $location.hash();
        }

     $http({
  method: 'GET',
  url: '/plugins/my'
}).then(function successCallback(response) {
   $scope.allPlugins = response.data[0].plugins;
  }, function errorCallback(response) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });

$scope.launchPlugin = function(item) {
  $state.go('run', {path: item.path, name: item.name});
}

        initialize();
    });


