'use strict';

angular.module('appApp')
    .controller('StoreCtrl', function ($scope, $q, $location, $http, $timeout) {

        function initialize() {
            var token = $location.hash();
            $scope.installedAlert = false;
        }

     $http({
  method: 'GET',
  url: '/plugins/'
}).then(function successCallback(response) {
   $scope.allPlugins = response.data;
   console.log($scope.allPlugins);
  }, function errorCallback(response) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });

$scope.installPlugin = function(item) {
	$http({
  		method: 'POST',
  		url: '/plugins/'+item._id+'/install'
		}).then(function successCallback(response) {
        $scope.installedAlert = true;
        $timeout(function() { $scope.installedAlert = false; }, 6000);
  		}, function errorCallback(response) {
  });
}

        initialize();
    });


