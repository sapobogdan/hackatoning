angular.module('banking.apps.uber', ['ui.router', 'leaflet-directive'])
    .config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');

        $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'index.html'
        })
        .state('map', {
            url: '/map',
            templateUrl: 'map.html'
        })
    })
    .directive('orderCab', function() {
        return {
            restrict: 'E',
            templateUrl: 'order.html',
            replace: true,
            controller: function($scope, $interval, $window, $timeout, leafletData) {

		$scope.called = false;
		$scope.timer = {
		    minutes: 0,
		    seconds: 60
		};

		carIcon = {
		    iconUrl: 'img/car.png',
		    //shadowUrl: 'examples/img/leaf-shadow.png',
		    iconSize:     [64, 64], // size of the icon
		    shadowSize:   [50, 64], // size of the shadow
		    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
		    shadowAnchor: [4, 62],  // the same for the shadow
		    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
		};

		$timeout(function() {
		    var windowHeight = angular.element($window).height();
		    var mapElem = angular.element('.angular-leaflet-map');
		    mapElem.height(windowHeight - 80);
		    leafletData.getMap('leafletMap').then(function(el) {

			el.invalidateSize();
		    });
		}, 1000);

		$scope.map = {
                    mapDefaults: {
                        scrollWheelZoom: false
                    },
                    geojson: {},
                    center: {
                        lat: 44.484940795017835,
                        lng: 26.10697567462921,
                        zoom: 15
                    },
		    markers: {
			you: {
			    lat: 44.484940795017835,
			    lng: 26.10697567462921,
			    focus: true,
			    message: "Call a cab here",
			    draggable: true
			},
			car1: {
			    lat: 44.484940795017835,
			    lng: 26.112098693847656,
			    message: "B-188-MHN",
			    draggable: false,
			    icon: carIcon
			}
		    }
                };

                $scope.callCab = function () {
		    $scope.called = true;
		    var decrease = function () {
			if ($scope.timer.seconds) {
			    $scope.timer.seconds--;
			}
		    };
		    $interval(decrease, 1000);
                };

            }
        }
    });
